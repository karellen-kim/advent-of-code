import scala.io.Source

/**
 * Created by karellen on 2016. 1. 3..
 */
object Day6 extends App {

  // toggle 461,550 through 564,900
  // turn off 370,39 through 425,839
  // turn on 599,989 through 806,993

  var lights = Array.ofDim[Boolean](1000, 1000)
  Source.fromURL(getClass.getResource("day6.txt")).mkString.split("\n").foreach(line => {
    val pattern = "(toggle|turn off|turn on) ([0-9]+),([0-9]+) through ([0-9]+),([0-9]+)".r
    val pattern(command, startX, startY, endX, endY) = line

    command match {
      case "toggle" => toggle(startX.toInt, startY.toInt, endX.toInt, endY.toInt)
      case "turn off" => onOff(startX.toInt, startY.toInt, endX.toInt, endY.toInt, false)
      case "turn on" => onOff(startX.toInt, startY.toInt, endX.toInt, endY.toInt, true)
    }
  })
  print(count())

  def onOff(startX : Int, startY : Int, endX : Int, endY : Int, onOff : Boolean): Unit = {
    (startX to endX).foreach(x => {
      (startY to endY).foreach(y => {
        lights(x)(y) = onOff
      })
    })
  }

  def toggle(startX : Int, startY : Int, endX : Int, endY : Int): Unit = {
    (startX to endX).foreach(x => {
      (startY to endY).foreach(y => {
        lights(x)(y) = !lights(x)(y)
      })
    })
  }

  def count(): Int = {
    var count = 0
    (0 to 999).foreach(x => {
      (0 to 999).foreach(y => {
        if (lights(x)(y)) count = count + 1
      })
    })
    count
  }
}

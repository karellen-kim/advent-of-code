import scala.io.Source

/**
 * Created by karellen on 2016. 1. 3..
 */
object Day5 extends App {

  var sentences = Source.fromURL(getClass.getResource("day5.txt")).mkString.split("\n")
  val niceStringCount = sentences.toSeq
    .filterNot(str => str.contains("ab") || str.contains("cd") || str.contains("pq") || str.contains("xy")) // not contain the strings ab, cd, pq, xy
    .filter(str => str.toCharArray.toSeq.foldLeft(0) { (acc : Int, ch : Char) => // at least three vowels (aeiou only)
        if (ch == 'a' || ch == 'e' ||  ch == 'i' || ch == 'o' || ch == 'u') acc + 1
        else acc
        } >= 3
    )
    .filter(str => { // at least one letter that appears twice in a row
      var find = false
      str.toCharArray.toSeq.foldLeft(' ') { (pre : Char, cur : Char) =>
        if (pre == cur) find = true
        cur
      }
      find
    }).size

  print(niceStringCount)
}

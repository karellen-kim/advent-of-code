import scala.io.Source

/**
 * Created by karellen on 2016. 1. 3..
 */
object Day3 extends App {

  var map = new scala.collection.immutable.HashMap[(Int, Int), Boolean]
  var curX = 0
  var curY = 0
  map = map + ((curX, curY) -> true)

  val visitedHouse = Source.fromURL(getClass.getResource("day3.txt")).mkString.toCharArray.foldLeft(1){
    (acc : Int, ch : Char) => {
      ch match {
        case '^' => curY = curY - 1
        case 'v' => curY = curY + 1
        case '<' => curX = curX - 1
        case '>' => curX = curX + 1
      }
      map.get(curX, curY) match {
        case Some(visited) => acc
        case None => {
          map = map + ((curX, curY) -> true)
          acc + 1
        }
      }
    }
  }

  print(visitedHouse)
}

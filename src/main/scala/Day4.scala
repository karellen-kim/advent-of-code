import java.security.MessageDigest

/**
 * Created by karellen on 2016. 1. 3..
 */
object Day4 extends App {

  var hash = ""
  var i = 0

  while(hash.startsWith("00000") == false) {
    i = i + 1
    hash = md5("ckczppom" + i)
  }
  println(i)
  println(hash)


  def md5(str : String) : String = {
    val digest = (MessageDigest.getInstance("MD5").digest(str.getBytes))
    digest.toSeq.foldLeft(""){
      (acc : String, b : Byte) => acc + Integer.toHexString((b & 0xFF) | 0x100).substring(1,3)
    }
  }
}

import scala.io.Source

/**
 * Created by karellen on 2016. 1. 3..
 */
object Day2 extends App {

  val wrappingPaperSize = Source.fromURL(getClass.getResource("day2.txt")).mkString.split("\n").foldLeft(0) {
    (acc : Int, line : String) => {
      val dim = line.split("x") match {
        case Array(l, w, h, _*) => (l.toInt, w.toInt, h.toInt)
      }

      val lw = dim._1 * dim._2
      val lh = dim._1 * dim._3
      val wh = dim._2 * dim._3

      acc + 2 * (lw + lh + wh) + Math.min(Math.min(lw, lh), wh)
    }
  }

  print(wrappingPaperSize)
}

import scala.io.Source

/**
 * Created by karellen on 2016. 1. 3..
 */
object Day1 extends App {

  val floor = Source.fromURL(getClass.getResource("day1.txt")).mkString.toCharArray.foldLeft(0){
    (acc : Int, ch : Char) => ch match {
      case '(' => acc + 1
      case ')' => acc - 1
    }
  }

  print(floor)
}
